#!/usr/bin/env bash
./bin/docker/pim-setup.sh
docker-compose up -d --build
./bin/docker/pim-dependencies.sh
./bin/docker/pim-initialize.sh
docker-compose exec fpm bash

<?php
declare(strict_types=1);

namespace Pcmt\Bundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;

class PcmtBundleExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        //load and merge configuration
        $configuration = $this->getConfiguration($configs, $container); //instantiate /DependencyInjection/Configuration class
        $config = $this->processConfiguration($configuration, $configs);
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config/'));
        $loader->load('services.yml');
    }

    public function getAlias()
    {
        return 'pcmtservice';
    }
}
<?php

namespace Pcmt\Bundle\Helper;

final class GsCodesHelper {

  public static function getGsCodes() {
    return [
      "AdditionalTradeItemClassificationCodeListCode",
      "AdditionalTradeItemIdentificationTypeCode",
      "ColourCodeListCode",
      "CountryCode",
      "DataCarrierTypeCode",
      "GDSNMeasurementUnitCode",
      "Gs1TradeItemIdentificationKeyCode",
      "ImportClassificationTypeCode",
      "LanguageCode",
      "NonfoodIngredientOfConcernCode",
      "PackageTypeCode",
      "PlatformTypeCode",
      "ReferencedFileTypeCode",
      "RegulationTypeCode",
      "RouteAdministration",
      "ShippingContainerTypeCode",
      "SizeCodeListCode",
      "TemperatureQualifierCode",
      "TradeItemUnitDescriptorCode"
    ];
  }
}
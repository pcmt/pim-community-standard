<?php
declare(strict_types=1);

namespace Pcmt\Bundle\Form;

use Pcmt\Bundle\Entity\Supplier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class SupplierAttributeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('code',TextType::class, [ 'constraints' => [ new NotBlank() ] ])
                ->add('name', TextType::class, ['constraints' => [ new NotBlank() ] ]);
        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Supplier::class
        ]);
    }
}
<?php
declare(strict_types=1);

namespace Pcmt\Bundle\PcmtEnrichBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PcmtEnrichBundle extends Bundle
{
}
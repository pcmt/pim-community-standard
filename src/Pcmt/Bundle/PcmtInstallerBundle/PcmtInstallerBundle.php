<?php
declare(strict_types=1);

namespace Pcmt\Bundle\PcmtInstallerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PcmtInstallerBundle extends Bundle
{
}
<?php
declare(strict_types=1);

namespace Pcmt\Bundle\PcmtConnectorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PcmtConnectorBundle extends Bundle
{
}
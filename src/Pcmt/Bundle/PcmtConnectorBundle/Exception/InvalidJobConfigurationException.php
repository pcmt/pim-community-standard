<?php
declare(strict_types=1);

namespace Pcmt\Bundle\PcmtConnectorBundle\Exception;

class InvalidJobConfigurationException extends \Exception
{
}